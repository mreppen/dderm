# Deep Dynamic Empirical Risk Minimization

This is code and log files for the experiments in the paper found at https://arxiv.org/abs/2011.09349

## Structure

The log files contain all parameters needed to fully reproduce the tables in JSON format, including the random generator seed.  Each log file directory contains files named `table[1-4]` to indicate in which tables of the paper the data appears.

`main.ml` is the file of the main application, with built-in man pages as documentation (see below).

`run_several.ml` is a convenience application to automatically run a sequence of computations.

## Setup

The code is written in the programming language OCaml.  To run it, you must first install OCaml using your operating system package manager or otherwise normal installation method.  You will also need OpenBLAS (typically installed with Julia/numpy/etc) and the OCaml package manager opam.  These packages should be available in any Linux distribution.  Note that Ubuntu's OpenBLAS package is/was a bit unusual.  See the Dockerfile for references on Ubuntu or use the [Owl Dockerfile](https://hub.docker.com/r/owlbarn/owl/).

For Windows and Mac users who lack proper package managers, a Dockerfile is made available as a reference point for how to run using Docker.  The numerical library Owl that is used also supplied centrally as an [Owl Dockerfile](https://hub.docker.com/r/owlbarn/owl/), which can be used instead.  NOTE: Mac cannot run Docker containers at native speed, so this could significantly slow down computations.  For Mac, OCaml/opam/OpenBLAS can be installed through homebrew for native execution.  I am not aware how Docker runs on Windows, but if it uses WSL, it potentially runs at native speeds.  Without Docker, running opam in WSL requires disabling sandboxing during the initial `opam init` command, due to the lack of WSL sandboxing package.

Once installed and set up (`opam init`, etc), run the following to install the dependencies (plplot itself must be installed separately, but is not strictly needed, and the corresponding lines can be removed from the code)
```
opam install dune owl owl-plplot cmdliner yojson shexp re
```

For interactive use, you likely also want to run `opam install utop owl-top`.  And for navigating the code, either set up merlin with your editor or an LSP server like with `opam install ocaml-lsp-server`.

## Usage

The `dune exec` commands below can be optimized by adding `--profile=release` flag after the exec subcommand.

Once everything is installed, the code can be run with
```
dune exec ./main.exe
```
or
```
dune exec ./run_several.exe
```

The main executable is documented and the documentation can be accessed by running
```
dune exec -- ./main.exe --help
```
or, as indicated after running the command above, for the specific `run` and `test` subcommands,
```
dune exec -- ./main.exe run --help
```

The automation program has less internal documentation, but should be mostly straight-forward.

## Troubleshooting

The code was originally run with the then-current versions of libraries and OCaml.  The OCaml ecosystem typically has great backward compatibility, so likely the code runs with current packages at the time you try it.

As the code started from small experiments, I did not pin the package versions.  If you have trouble (or I missed a dependency), please contact me and I will figure out with library versions were originally installed.


