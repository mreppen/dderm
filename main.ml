open Nn_main
open Nn_main.Main
open Nn_main.Logging
open Cmdliner

let () =
  let defalt_sample_size = 100000 in
  let default_dims = 20 in
  let default_widths = [| 10; 10; 10; 10 |] in
  let default_batch_size = 32 in

  let sample_size = Arg.(value & opt int defalt_sample_size & info ["sample-size"] ~docv:"INTEGER") in
  let directory = Arg.(value & opt dir "." & info ["d"; "dir"; "directory"; "rundir"] ~doc:"Data directory.") in

  let main = Term.(const (fun () -> print_endline "Try --help for help") $ const ()) in

  let run =
    let train_seed = Arg.(value & opt string "Reppen" & info ["seed"] ~docv:"STRING" ~doc:"Seed for training. Does not default to self-init.") in
    let widths = Arg.(value & opt (array ~sep:';' int) default_widths & info ["widths"] ~doc:"Widths if architecture file cannot be read.") in
    let dims = Arg.(value & opt int default_dims & info ["dimensions"] ~doc:"Dimensions if architecture file cannot be read.") in
    let layer_init = Arg.(value & opt string "Standard" & info ["layer-init"] ~docv:"STRING" ~doc:"Type of initializer for the nn layer weights") in
    let batch_size = Arg.(value & opt int default_batch_size & info ["batch-size"] ~doc:"Size of mini-batches. Full batches if zero.") in
    let notest = Arg.(value & flag & info ["no-test"] ~docv:"Skips the performance test if present") in
    Term.(const
      (fun ssize dims batch_size init seed widths dir notest ->
        Sys.chdir dir;
        let init =
          match init with
          | "He" -> Nnet.He
          | "Standard" -> Nnet.Standard
          | _ -> failwith "Invalid layer initializer argument"
        in
        let (module Model) = (module Experiments.Utility_optimization_one ( Params ( struct let dims = dims end ) ) : Nnet.Model ) in
        let (module Main) = (module Make (Model : Nnet.Model) (Params ( struct let dims = dims end )) : MAIN_SIG) in
        let batch_size = if batch_size = 0 then ssize else batch_size in
        let tr_obj = Main.train ~widths ~train_seed:Seed.(String seed) ~init ~batch_size ssize in
        if not notest then
          Main.Trainer.(test_performance ~sample_size:ssize ~seed:Seed.Self tr_obj |> print_testlog))
      $ sample_size $ dims $ batch_size $ layer_init $ train_seed $ widths $ directory $ notest)
  in
  let test =
    let seed = Arg.(value & opt string "" & info ["seed"] ~docv:"STRING" ~doc:"Seed for testing. Defaults to self-init.") in
    let logfile = Arg.(value & opt string "" & info ["o"]) in
    Term.(const
      (fun ssize seed dir logfile ->
        Sys.chdir dir;
        let seed = match seed with "" -> Seed.Self | x -> Seed.String x in
        Seed.run seed;
        let trainlog_file = "trainlog.json" in
        let train_log =
          match Logging.trainlog_of_yojson @@ Yojson.Safe.from_file trainlog_file with
          | Ok tl -> tl
          | Error s -> failwith ("Could not parse logfile " ^ s)
        in
        let dims = train_log.params.dimensions in
        let (module Model) = (module Experiments.Utility_optimization_one ( Params ( struct let dims = dims end ) ) : Nnet.Model ) in
        let (module Main) = (module Make (Model : Nnet.Model) (Params ( struct let dims = dims end )) : MAIN_SIG) in
        let tr_obj = Main.Trainer.load () |> Option.get in
        let tl_json =
          Main.Trainer.test_performance ~seed ~sample_size:ssize tr_obj
          |> Logging.testlog_to_yojson
        in
        if logfile = ""
        then prerr_endline @@ Yojson.Safe.to_string tl_json
        else Yojson.Safe.to_file logfile tl_json)
      $ sample_size $ seed $ directory $ logfile)
  in
  Term.exit @@
  Term.eval_choice
  (main, Term.info "main.exe")
  [ (run, Term.info "run")
  ; (test, Term.info "test") ]
