(*
#!/usr/bin/env ocaml
#use "topfind"
#require "ppx_deriving_yojson"
#mod_use "../lib/logging.ml"
#thread
#require "shexp.process"
#require "cmdliner"
#require "re"
*)
open Shexp_process
open Infix
module List = Stdlib.List
module Logging = Nn_main.Logging

let spf = Printf.sprintf

let executable =
  let dir = Filename.dirname Sys.argv.(0) in
  dir ^ "/main.exe"

type summary =
  { train : Logging.trainlog
  ; tests : Logging.testlog list } [@@deriving yojson]

let is_dir dir =
  try
    let f = Unix.stat dir in f.st_kind = S_DIR
  with
    Unix.Unix_error(Unix.ENOENT, _, _) -> false

let read_dir dir =
  let cur_dir = Unix.opendir dir in
  let rec read_dir acc =
    try
      let f = Unix.readdir cur_dir in
      read_dir (f::acc)
    with End_of_file -> acc
  in
  let l = read_dir [] in
  Unix.closedir cur_dir;
  l


let train dir seed dims ssize batch_size w_initializer widths =
  let batch_size_cmd = if batch_size = 0 then [] else [ spf "--batch-size=%i" batch_size ] in
  if is_dir dir
  then return true
  else (
    eval (mkdir dir);
    set_env "OPENBLAS_NUM_THREADS" "1" @@
    set_env "OMP_NUM_THREADS" "1" @@
    run_bool executable
    ( [ "run"
      ; spf "--directory=%s" dir
      ; spf "--dimensions=%i" dims
      ; spf "--seed=%s" seed
      ; spf "--sample-size=%i" ssize
      ; spf "--widths=%s" (Array.map string_of_int widths |> Array.to_list |> String.concat ";")
      ; spf "--layer-init=%s" w_initializer
      ; "--no-test" ]
    @ batch_size_cmd )
    |> stderr_to (dir ^ "/trainlog")
    |> stdout_to "/dev/null")

let test dir ssize =
  let seed = Random.bits () |> string_of_int in
  let logfile = spf "testlog-%s" seed in
  let abs_logfile = spf "%s/%s" dir logfile in
  run executable
  [ "test"
  ; spf "--directory=%s" dir
  ; spf "--seed=%s" seed
  ; spf "--sample-size=%i" ssize
  ; "-o"; spf "%s.json" logfile ]
  |> stderr_to abs_logfile
  |> stdout_to abs_logfile

let get_logs dir =
  let (let*) = Result.bind in
  let* trainlog = Logging.trainlog_of_yojson @@
    Yojson.Safe.from_file (dir ^ "/trainlog.json") in
  let testlog_re = Re.(seq [str "testlog-"; rep1 digit; str ".json"] |> whole_string |> compile) in
  let tests =
    read_dir dir
    |> List.filter (Re.execp testlog_re)
    |> List.map ((^) (dir ^ "/"))
    |> List.filter_map (fun s ->
        match Logging.testlog_of_yojson @@ Yojson.Safe.from_file s with
        | Ok tl -> Some tl
        | Error s -> prerr_endline ("Could not parse " ^ s); None)
  in
  Ok { train=trainlog; tests }

let () =
  let open Cmdliner in
  let widths = [| 10; 10; 10 |] in
  let start = Arg.(value & opt int 1 & info ["start"]) in
  let stop = Arg.(value & opt int 1 & info ["stop"]) in
  let step = Arg.(value & opt int 1 & info ["step"]) in
  let dir_prefix = Arg.(value & opt string "" & info ["dir-prefix"]) in
  let ssize = Arg.(value & opt int 40000 & info ["sample-size"]) in
  let batch_size = Arg.(value & opt int 0 & info ["batch-size"]) in
  let dims = Arg.(value & opt int 1 & info ["dimensions"; "dims" ]) in
  let variable = Arg.(value & opt string "run" & info ["variable"; "var"]) in
  let w_initializer = Arg.(value & opt string "Standard" & info ["layer-init"]) in

  Random.self_init ();

  let main = Term.(const (fun () -> print_endline "Must be run from a subdirectory of the project root.\nPass --help for help") $ const ()) in

  let train =
    let nparamsdim = Arg.(value & opt (some int) None & info ["nparams-dim"]) in
    Term.(const (fun variable start step stop dir_prefix dims ssize batch_size w_initializer nparamsdim ->
      let dir_prefix = if dir_prefix = "" then variable else dir_prefix in
      let width1 dims =
        match nparamsdim with
        | None -> widths.(0)
        | Some x -> max widths.(0) @@ Float.to_int @@ Float.round ( float_of_int (x * widths.(1) + (widths.(0) + 1) * widths.(1)) /. float_of_int (widths.(0) + 1 + dims))
      in
      let rec run_loop_next var =
        if var <= stop
        then
          match variable with
          | "dims" -> loop var var ssize
          | "ssize" -> loop var dims var
          | "run" -> loop var dims ssize
          | _ -> failwith "Unspecified iteration variable"
      and loop var dims ssize =
        let tic = Unix.time () in
        Printf.printf "%s=%i: " variable var;

        let widths_adj = Array.copy widths in
        widths_adj.(0) <- width1 dims;
        let dir = Printf.sprintf "%s%03i" dir_prefix var in (* TODO: format problem for ssize *)
        let seed = dir in
        let cmd = train dir seed dims ssize batch_size w_initializer widths_adj
        >>= fun suc ->
          if suc then return (ignore (test dir ssize)) else print "Training failed"
        in
        Shexp_process.eval cmd;

        let gm = Unix.(time () -. tic |> gmtime) in
        Printf.printf "%im%is\n" gm.tm_min gm.tm_sec;
        flush stdout;

        run_loop_next (var + step)
      in run_loop_next start
      )
    $ variable $ start $ step $ stop $ dir_prefix $ dims $ ssize $ batch_size $ w_initializer $ nparamsdim)
  in

  let test =
    Term.(const (fun variable start step stop dir_prefix ssize ->
      let dir_prefix = if dir_prefix = "" then variable else dir_prefix in
      let rec run_loop_next var =
        if var <= stop
        then
          match variable with
          | "dims" -> loop var
          | "ssize" -> loop var
          | "run" -> loop var
          | _ -> failwith "Unspecified iteration variable"
      and loop var =
        let dir = Printf.sprintf "%s%03i" dir_prefix var in
        Shexp_process.eval (test dir ssize);
        run_loop_next (var + step)
      in run_loop_next start)
    $ variable $ start $ step $ stop $ dir_prefix $ ssize)
  in

  let summarize =
    let dirs = Arg.(value & pos_all string [] & info []) in
    Term.(const (fun dir_prefix dirs ->
      let module List = Stdlib.List in
      let subdirs =
        let regex = Re.(seq [bol; str dir_prefix; rep1 digit; eol] |> compile) in
        List.filter is_dir (read_dir ".")
        |> List.filter (Re.execp regex)
      in
      let dirs = (List.filter is_dir dirs) @ List.sort String.compare subdirs in
      let res = List.fold_left
        (fun acc dir ->
          match get_logs dir with
          | Ok logs -> logs::acc
          | Error x -> prerr_endline x; acc)
        [] dirs
        |> List.rev
      in
      Printf.printf "%4s, %8s, %13s, %7s, %6s, %25s, %24s, %14s, %13s\n" "dims" "ssize" "rel_perf_diff" "nparams" "epochs" "cert_equiv_rel_true_train" "cert_equiv_rel_true_diff" "perf_rel_train" "perf_rel_test";
      List.iter
      (fun { train; tests } ->
        List.iter
        (fun (testl : Logging.testlog) ->
          Printf.printf "%4i, %8i, %13f, %7i, %6i, %25f, %24f, %14f, %13f\n"
          train.params.dimensions
          train.params.sample_size
          (testl.test.rel_perf -. testl.train.rel_perf)
          train.params.nparams
          Float.(to_int @@ of_int train.batches /. (of_int train.params.sample_size /. of_int train.params.batch_size))
          (100. *. log (1. +. testl.train.rel_perf /. 100.) /. log testl.train.true_perf)
          (100. *. ((log (1. +. testl.train.rel_perf /. 100.) /. log testl.train.true_perf) -. (log (1. +. testl.test.rel_perf /. 100.) /. log testl.test.true_perf)))
          testl.train.rel_perf
          testl.test.rel_perf)
        tests)
      res)
    $ dir_prefix $ dirs)
  in

  Term.exit @@
  Term.eval_choice (main, Term.info "run_several")
  [ (train, Term.info "train")
  ; (test, Term.info "test")
  ; (summarize, Term.info "summarize") ]
