FROM debian AS owl_run

RUN apt-get update
RUN apt-get install -y liblapacke-dev libopenblas-dev pkg-config
RUN apt-get install -y libplplot-dev
RUN apt-get install -y sudo

RUN useradd -m -s /bin/bash max
USER max
RUN mkdir /home/max/workdir
WORKDIR /home/max/workdir


FROM owl_run as owl_compile
USER root
RUN apt-get install -y opam mccs
USER max

RUN opam init -a --solver=mccs --disable-sandboxing
RUN opam switch create owl 4.10.0+flambda --solver=mccs
RUN opam switch owl
RUN eval $(opam env)
RUN opam install -y dune owl owl-plplot

ENV BASH_ENV=~/.profile
RUN echo 'eval $(opam env)' >> ~/.bashrc
