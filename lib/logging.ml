type stopping_rule =
  | OOS_EPOCH of int * int * float (* oos_size * patience * tolerance *)
  | OVERPERF of float * int
  | MAX_EPOCHS of float
  [@@deriving yojson]
type layer_init =
  | Standard
  | He
  [@@deriving yojson]
type trainparams =
  { widths : int array
  ; dimensions : int
  ; layer_init : layer_init
  ; sample_size : int
  ; nparams : int
  ; seed : string option
  ; stopping_rule : stopping_rule
  ; batch_size : int
  } [@@deriving yojson]
type trainlog =
  { params : trainparams
  ; epochs : float
  ; batches : int
  ; loss : float
  ; true_loss : (float [@default infinity])
  ; time : float
  } [@@deriving yojson]

let sprint_trainparams tp =
  Printf.sprintf "Training parameters:\n  widths = %s\n  dimensions = %i\n  sample size = %i\n  seed = %s\n  #params = %i\n"
    (Array.map string_of_int tp.widths |> Array.to_list |> String.concat "; ")
    tp.dimensions
    tp.sample_size
    (Option.value ~default:"unknown" tp.seed)
    tp.nparams

let sprint_trainres tl =
  Printf.sprintf "Training results:\n  epochs = %f\n  loss = %f\n  time = %f\n"
    tl.epochs
    tl.loss
    tl.time

let sprint_trainlog tl =
  sprint_trainparams tl.params
  ^ sprint_trainres tl

type performance =
  { nn_perf : float
  ; true_perf : float
  ; rel_perf : float
  ; sample_size : int
  } [@@deriving yojson]

type testlog =
  { train : performance
  ; test : performance
  } [@@deriving yojson]

let print_testlog {train; test} =
  let out = ref "" in
  let print s = out := !out ^ s in
  print "Sample performance for the nn optimizer (in-sample, out-of-sample):\n";
  print @@ Printf.sprintf "  %f\n  %f\n" train.nn_perf test.nn_perf;
  print "Sample performance for the true optimizer (in-sample, out-of-sample):\n";
  print @@ Printf.sprintf "  %f\n  %f\n" train.true_perf test.true_perf;
  print "Performance difference (in-sample - out-of-sample):\n";
  print @@ Printf.sprintf "  %f\n" (train.nn_perf -. test.nn_perf);
  print "Relative performance (in-sample rel, out-of-sample rel):\n";
  print @@ Printf.sprintf "  %f %%\n  %f %%\n" train.rel_perf test.rel_perf;
  print "Relative performance difference (in-sample - out-of-sample):\n";
  print @@ Printf.sprintf "  %f %%\n" (train.rel_perf -. test.rel_perf);
  print_string !out
