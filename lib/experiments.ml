open Owl
open Neural.D

module Utility_optimization (Params : sig
  val dims : int
  val a : float
  val b : float
  val mu : int -> float
  val sigma : int -> float
end) : Nnet.Model = struct
  include Params
  let noise_accum = Nnet.Geometric
  let action_accum = Nnet.Nothing (* Would be Arithmetic, but with exp utility, it does not matter *)
  let control_out_dim ~dims = dims

  let true_optimizer z =
    let dims = (Arr.shape z).(0) in
    Arr.init [| dims |] (fun i -> (mu i) /. Float.pow (sigma i) 2.)

  let dynamics a _ x2 = Arr.(a * x2)

  let obj_fun_avg ~dims:_ _ y' =
    Arr.sum ~axis:1 y'
    |> Arr.map (fun y -> exp (-.y))
    |> Arr.mean'

  let training_objective ~dims:_ _ y' =
    Algodiff.Maths.(sum ~axis:1 y' |> mul (F (-1.)) |> exp |> sum') (* TODO: slow due to memory allocs *)

  let generate_x ~dims n =
    Arr.init_nd [| n; dims; 2 |] (fun ind ->
      let dim = ind.(1) in
      if ind.(2) = 0
      then Arr.get (Arr.uniform ~a ~b [| 1 |]) [| 0 |]
      else Arr.get (Arr.gaussian ~mu:(mu dim) ~sigma:(sigma dim) [| 1 |]) [| 0 |])
  
  let generate_y ~dims x =
    let n = (Arr.shape x).(0) in
    Arr.zeros [| n; dims |]
end

module Utility_optimization_one (Params : sig
  val dims : int
  val a : float
  val b : float
  val mu : int -> float
  val sigma : int -> float
end) : Nnet.Model = struct
  include Params
  let noise_accum = Nnet.Nothing
  let action_accum = Nnet.Nothing (* Would be Arithmetic, but with exp utility, it does not matter *)
  let control_out_dim ~dims:_ = 1

  let true_optimizer _ =
    Arr.init [| 1 |] (fun i -> (mu i) /. Float.pow (sigma i) 2.)

  let dynamics a _ _ = a

  let obj_fun_avg ~dims:_ z2 a =
    Arr.reshape a [| (Arr.shape a).(0); 1 |]
    |> Arr.mul z2
    |> Arr.map (fun y -> exp (-.y))
    |> Arr.mean'

  let training_objective ~dims:_ z2 a =
    let a = Algodiff.Maths.reshape a [| (Algodiff.shape a).(0); 1 |] in
    Algodiff.Maths.(mul z2 a |> mul (F (-1.)) |> exp |> sum')

  let generate_x ~dims n =
    Arr.init_nd [| n; dims; 2 |] (fun ind ->
      if ind.(2) = 0
      then Arr.get (Arr.uniform ~a ~b [| 1 |]) [| 0 |]
      else 0.)
  
  let generate_y ~dims:_ x =
    let n = (Arr.shape x).(0) in
    Arr.gaussian ~mu:(mu 0) ~sigma:(sigma 0) [| n; 1 |]
end

module Utility_optimization_one_nonlinear (Params : sig
  val dims : int
  val a : float
  val b : float
  val mu : int -> float
  val sigma : int -> float
end) : Nnet.Model = struct
  include Params
  let noise_accum = Nnet.Nothing
  let action_accum = Nnet.Nothing (* Would be Arithmetic, but with exp utility, it does not matter *)
  let control_out_dim ~dims:_ = 1

  let nonlinearity_1d z = ((abs_float a +. abs_float b) /. 2.) *. sin (Const.pi2 *. z)
  let true_optimizer z =
    Arr.init [| 1 |] (fun i -> (nonlinearity_1d Arr.(get (reshape z [| numel z |]) [|0|]) +. mu i) /. Float.pow (sigma i) 2.)

  let dynamics a _ _ = a

  let obj_fun_avg ~dims:_ z2 a =
    Arr.reshape a [| (Arr.shape a).(0); 1 |]
    |> Arr.mul z2
    |> Arr.map (fun y -> exp (-.y))
    |> Arr.mean'

  let training_objective ~dims:_ z2 a =
    let a = Algodiff.Maths.reshape a [| (Algodiff.shape a).(0); 1 |] in
    Algodiff.Maths.(mul z2 a |> mul (F (-1.)) |> exp |> sum')

  let generate_x ~dims n =
    Arr.init_nd [| n; dims; 2 |] (fun ind ->
      if ind.(2) = 0
      then Arr.get (Arr.uniform ~a ~b [| 1 |]) [| 0 |]
      else 0.)
  
  let generate_y ~dims:_ x =
    let n = (Arr.shape x).(0) in
    Arr.(init_nd [| n; 1 |] (fun ind ->
      let w = get (gaussian ~mu:(mu 0) ~sigma:(sigma 0) [| 1 |]) [| 0 |] in
      w +. nonlinearity_1d x.%{ind.(0); 0; 0}))
end

module Production_example (Params : sig
  val dims : int
  val a : float
  val b : float
end) : Nnet.Model = struct
  include Params
  let dims = Params.dims
  let noise_accum = Nnet.Nothing
  let action_accum = Nnet.Nothing
  let control_out_dim ~dims:_ = 1
  let true_optimizer_1d z = ((abs_float a +. abs_float b) /. 2.) *. sin (Const.pi2 *. z)

  let dist_t0 x = Arr.uniform ~a ~b x
  let dist_t1 x = Arr.gaussian x

  let true_optimizer z =
    let first_dim =
      Arr.(get (reshape z [|numel z|]) [|0|])
      |> true_optimizer_1d
    in
    let dims = (Arr.shape z).(1) in
    let o = Array.make dims 0. in
    o.(0) <- first_dim;
    Arr.of_array o [| dims |]

  let dynamics a _ _ = a (* impact of x is already in labels *)

  let noise_transf z = Arr.mean' z

  let obj_fun_avg ~dims y y' =
      Arr.(l2norm_sqr' (y - y'))
      |> (fun yy -> yy /. (Int.to_float (Arr.numel y)))
      |> (fun yy -> yy *. (Int.to_float dims))

  let training_objective ~dims =
    let dims_scaling = Int.to_float dims in
    fun y y' ->
      Algodiff.(Maths.(l2norm_sqr' (y - y')
      |> mul (F dims_scaling)))

  let generate_x ~dims n =
    Arr.concatenate ~axis:2
      [| dist_t0 [| n; dims; 1 |]
      ;  dist_t1 [| n; dims; 1 |] |]
  
  let generate_y ~dims:_ x =
    let n = (Arr.shape x).(0) in
    Arr.(
      of_array
      (map_slice ~axis:0
        (fun xx -> (Arr.sum' @@ true_optimizer xx.${[]; [0]}) +. noise_transf xx.${[]; [1]})
        x)
      [| n; 1 |])
end

