open Owl
module Graph = Neural.D.Graph

let log s = prerr_endline s; flush stderr

let plot_loss ?(fname="losses.png") losses =
  let module Plot = Owl_plplot.Plot in
  let h = Plot.create fname in
  Plot.plot ~h (Mat.sequential 1 (Array.length losses)) (Mat.of_arrays [| losses |]);
  Plot.output h


let plot_control ?(dim=0) ?(fname="plot.png") domain_gen control_nn true_optimizer =
  let open Owl_plplot.Plot in
  let inp_shape = Graph.input_shape control_nn in
  let dspace = domain_gen 1000 in
  let fullspace = Arr.zeros (Array.append [| 1000 |] inp_shape) in
  Arr.(fullspace.${[]; [dim]} <- reshape dspace [| 1000; 1; 1 |]);
  let control = Graph.model control_nn fullspace in
  let h = create fname in
  plot ~h ~spec:[RGB (255, 0, 0); Axis Y] dspace control;
  let true_function_dim x =
    true_optimizer @@
    Arr.init_nd inp_shape
    (fun ia -> let i = ia.(0) in if i = dim then x else 0.)
    |> fun x -> Arr.get x [|0|]
  in
  let a = Arr.get dspace [| 0 |]
  and b = Arr.get dspace [| 999 |] in
  plot_fun ~h ~spec:[RGB (0, 0, 255)] true_function_dim a b;
  output h

let save_nn ({ arch; nn; _ } : Training.t)  =
  Graph.save_weights nn Training.weights_file;
  Nnet.arch_to_yojson arch |> Yojson.Safe.to_file Training.nn_arch_file;
  log "Saved nn"

let append_losses filename losses =
  let len = Array.length losses in 
  let losses = Arr.of_array losses [| len |] in
  (*
  let losses = Arr.of_array (Array.map Algodiff.unpack_flt losses) [| len |] in*)
  match Arr.load filename with
    | exception Sys_error _ ->
        Arr.save ~out:filename losses
    | x ->
        Arr.concat_horizontal x losses
        |> Arr.save ~out:filename
