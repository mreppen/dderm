type t =
      Continue
    | Self
    | String of string

let run = function
  | Continue -> ()
  | Self -> Owl_stats_prng.self_init ()
  | String s ->
    let hexdigest = "0x" ^ (Digest.(string s |> to_hex) |> fun s -> String.sub s 0 15) in
    Owl_stats_prng.init (hexdigest |> int_of_string)
