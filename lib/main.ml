open Owl
open Logging
module Graph = Nnet.Graph

module type PARAMS = sig
  val dims : int
  val plot_domain : int -> Arr.arr
end

module Params (Dims : sig val dims : int end) = struct
  include Dims
  let a = -0.5
  let b = 0.5
  let mu = fun i -> 0.20 *. (0.9 -. Float.of_int i /. Float.of_int dims)
  let sigma = fun i -> 0.4 *. (1.1 -. Float.of_int i /. Float.of_int dims)
  let plot_domain n = Arr.linspace a b n
end

module type MAIN_SIG = sig
  module Trainer : Training.TRAINING
  val train : widths:int array -> train_seed:Seed.t -> init:layer_init -> batch_size:int -> int -> Training.t
end
  

module Make (Model : Nnet.Model) (Params : PARAMS) : MAIN_SIG = struct
  module Model = Model
  module NN = Nnet.Make (Model)
  module Data = Nnet.Data
  module Trainer = Training.Make (Model)
  let dims = Params.dims
 
  module type STOPPING_RULE_SIG = sig
    type t
    val checkpoint_function : t -> Graph.network -> int -> Nnet.Optimise.Checkpoint.typ
    val log : t -> Logging.stopping_rule
    val finish : t -> Nnet.Optimise.Checkpoint.state -> unit
    val save : t -> string -> unit
  end

  module type MAX_EPOCH_STOPPING = sig
    include STOPPING_RULE_SIG with type t = float
    val [@warning "-32"] make : epochs:float -> t
    end
  module Max_epochs_stopping : MAX_EPOCH_STOPPING = struct
    type t = float
    let make ~epochs = epochs
    let log epochs = MAX_EPOCHS epochs
    let checkpoint_function epochs = Trainer.max_epochs_stopping ~epochs
    let finish _ _ = ()
    let save _ _ = ()
  end

  module type OOS_EPOCH_STOPPING = sig
      include STOPPING_RULE_SIG with type t = int * float * int * float array ref
      val [@warning "-32"] make : patience:int -> tolerance:float -> ssize:int -> oos_losses:(float array ref) -> t
    end
  module Oos_epoch_stopping : OOS_EPOCH_STOPPING = struct
    type t = int * float * int * float array ref
    let make ~patience ~tolerance ~ssize ~oos_losses = (patience, tolerance, ssize, oos_losses)

    let log (patience, tolerance, ssize, _oos_losses) = OOS_EPOCH(ssize, patience, tolerance)

    let checkpoint_function (patience, tolerance, ssize, oos_losses) = Trainer.oos_epoch_stopping ~patience ~tolerance ~oos_size:ssize oos_losses

    let finish (_patience, _tolerance, _ssize, oos_losses) (train_state : Nnet.Optimise.Checkpoint.state) =
      let epoch = train_state.current_batch / train_state.batches_per_epoch in
      let trimmed_losses =
        Array.sub !oos_losses 1 epoch
        |> Array.map (fun x -> if x = Float.infinity then 0. else x )
      in
      oos_losses := trimmed_losses

    let save (_patience, _tolerance, _ssize, oos_losses) prepend =
      Output.append_losses (prepend ^ "oos_losses.bin") !oos_losses;
      Output.plot_loss ~fname:(prepend ^ "oos_losses.png") !oos_losses
  end

  module type OVERPERFORMANCE_STOPPING = sig
      include STOPPING_RULE_SIG with type t = float * int * Nnet.Data.t
      val [@warning "-32"] make : overperformace:float -> win:int -> train_data:Nnet.Data.t -> t
    end
  module Overperformance_stopping : OVERPERFORMANCE_STOPPING = struct
    type t = float * int * Nnet.Data.t
    let make ~overperformace ~win ~train_data = overperformace, win, train_data
    let log (overperformance, win, _data) = OVERPERF(overperformance, win)
    let checkpoint_function (overperformance, win, train_data) = Trainer.overperformance_stopping ~overperformance ~win ~train_data
    let finish _ _ = ()
    let save _ _ = ()
  end

  let train ~widths ~train_seed ~init:layer_init ~batch_size sample_size =
    Seed.run train_seed;
    let open Trainer in
    let ({ arch; nn; train_data=train_data } : Training.t) as train_obj =
      match load ~arch_if_fail:{dims; widths; init=layer_init} () with
      | Some x -> x
      | None ->
          print_endline "Reinitializing data and neural network";
          init ~widths ~init:layer_init sample_size
    in
  (*  let (module Stopping_rule : OVERPERFORMANCE_STOPPING) = (module Overperformance_stopping)
    and stopping_rule = Overperformance_stopping.make ~overperformace:0.02 ~win:(sample_size / batch_size) ~train_data:train_obj.train_data in*)
    let (module Stopping_rule : OOS_EPOCH_STOPPING) = (module Oos_epoch_stopping)
    and stopping_rule = Oos_epoch_stopping.make ~patience:5 ~tolerance:0.01 ~ssize:100000 ~oos_losses:(ref [||]) in
    (*let (module Stopping_rule : MAX_EPOCH_STOPPING) = (module Max_epochs_stopping)
    and stopping_rule = Max_epochs_stopping.make ~epochs:100. in*)
    let tp =
      { widths=arch.widths
      ; dimensions=arch.dims
      ; layer_init=arch.init
      ; sample_size
      ; nparams=(Nnet.count_params nn)
      ; seed=(match train_seed with String x -> Some x | _ -> None)
      ; stopping_rule=Stopping_rule.log stopping_rule
      ; batch_size } in
    prerr_string @@ sprint_trainparams tp;
    flush stderr;

    let tic = Sys.time () in
    let train_state = train_ (Stopping_rule.checkpoint_function stopping_rule) batch_size train_obj in
    let toc = Sys.time () in
    Output.save_nn train_obj;
    Stopping_rule.finish stopping_rule train_state;
    let perf = performance dims nn train_data in
    let tl =
      { params=tp
      ; epochs=Float.(of_int train_state.current_batch /. of_int train_state.batches_per_epoch)
      ; batches=train_state.current_batch
      ; loss=perf.nn_perf
      ; true_loss=perf.true_perf
      ; time=(toc -. tic) }
    in
    prerr_string @@ sprint_trainres tl;
    Yojson.Safe.to_file "trainlog.json" (trainlog_to_yojson tl);

    let losses = Array.sub train_state.loss 0 train_state.current_batch |> Array.map Nnet.Algodiff.unpack_flt in
    Output.append_losses "losses.bin" losses;
    Output.plot_loss ~fname:"losses.png" losses;

    Stopping_rule.save stopping_rule "";

    Output.plot_control Params.plot_domain Graph.(get_subnetwork ~make_inputs:[| "d_1" |] (get_node nn "a_1")) Model.true_optimizer;

    train_obj
end

