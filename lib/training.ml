open Owl
open Nnet

let nn_arch_file = "nn_arch.bin"
let weights_file = "nn_weights.bin"
let xname = "train_data.bin"
and yname = "label_data.bin"

type t =
  { arch : Nnet.arch
  ; nn : Graph.network
  ; train_data : Nnet.Data.t }

module type TRAINING = sig
  module Model : Nnet.Model

  val performance : int -> Nnet.Graph.network -> Data.t -> Logging.performance
  val test_performance : ?sample_size:int -> seed:Seed.t -> t -> Logging.testlog
  val load : ?arch_if_fail:Nnet.arch -> unit -> t option
  val init : widths:int array -> init:layer_init -> int -> t
  val max_epochs_stopping : epochs:float -> Nnet.Graph.network -> int -> Nnet.Optimise.Checkpoint.typ
  val oos_epoch_stopping : patience:int -> tolerance:float -> oos_size:int -> float array ref -> Nnet.Graph.network -> int -> Nnet.Optimise.Checkpoint.typ
  val overperformance_stopping : overperformance:float -> win:int -> train_data:Nnet.Data.t -> Nnet.Graph.network -> int -> Nnet.Optimise.Checkpoint.typ

  val train_ : (Nnet.Graph.network -> int -> Neural.D.Checkpoint.typ) -> int -> t -> Neural.D.Checkpoint.state
end

module Make (Model : Nnet.Model) : TRAINING = struct
(*module Model = Nnet.Production_example (Params)*)
  module Model = Model
  let dims = Model.dims
  module NN = Nnet.Make (Model)
open Logging

let log s = prerr_endline s; flush stderr
(*let print = print_endline*)

let generate_data ~dims = NN.Data_gen.generate_data ~dims



let load ?arch_if_fail () =
  let (let+) x f = Option.map f x in
  let+ train_data = Data.load_from_files xname yname in
  log @@ Printf.sprintf "Loaded training (x, y) from (%s, %s)" xname yname;
  let arch, nn =
    match NN.load weights_file nn_arch_file with
    | Ok (arch, nn) ->
        log @@ "Loaded weights from " ^ weights_file ^ " and " ^ nn_arch_file;
        arch, nn
    | Error (`File_not_found s) ->
        let msg = "File not found: " ^ s in
        (match arch_if_fail with
        | Some ({ dims; widths; init } as arch) ->
            log msg; log "Initializing nn instead...";
            let nn = NN.make ~dims ~init widths in
            Graph.init nn; arch, nn
        | None ->
          failwith msg)
    | Error (`Parse_error s) ->
        failwith ("Failed to parse " ^ s)
    | Error `Wrong_arch ->
        failwith ("Network architecture and weights do not match?" ^ "\nAborting")
  in
  { arch; nn; train_data }

let init ~widths ~init sample_size =
  let nn = NN.make ~dims ~init widths in
  Graph.init nn;
  let train_data = generate_data ~dims sample_size in
  Data.save_to_files train_data xname yname;
  log "Generated training data";
  { arch={ dims; widths; init }; nn; train_data }


let performance dims nn (data : Nnet.Data.t) =
  let obj_fun_avg = Model.obj_fun_avg ~dims in
  let nn_perf = obj_fun_avg data.y @@ Graph.model nn data.x in
  let true_perf =
    obj_fun_avg data.y @@ Arr.(of_arrays
      (map_slice
        (fun x -> Model.(dynamics (reshape (true_optimizer x.${[]; [0]}) [| (shape data.y).(1); 1 |]) x.${[]; [0]} x.${[]; [1]}) |> to_array) data.x))
  in
  let rel_perf = (fun f1 f2 ->
    100. *. (f1 -. f2) /. f2)
    nn_perf true_perf
  in
  { nn_perf; true_perf; rel_perf; sample_size=Arr.row_num data.y }

let overperformance_stopping ~overperformance ~win ~train_data nn _ =
  let { true_perf; _ } = performance dims nn train_data in
  let perf_condition = (1. -. overperformance) *. true_perf in
  Optimise.Checkpoint.Custom
  (fun (state : Optimise.Checkpoint.state) ->
    if win < state.current_batch
    then begin
      let rec window_loss cur stop acc =
        let l = Algodiff.unpack_flt state.loss.(cur) in
        if cur < stop
        then window_loss (cur + 1) stop (acc +. l)
        else acc +. l
      in
      let l = (window_loss (state.current_batch + 1 - win) state.current_batch 0.) /. Float.of_int win in
      if l < perf_condition || (state.current_batch > 500 * state.batches_per_epoch && 0.01 *. true_perf > Algodiff.unpack_flt state.loss.(state.current_batch - 500 * state.batches_per_epoch) -. l) 
      then state.stop <- true
    end)

let max_epochs_stopping ~epochs _nn epochs_bound =
  if epochs > Float.of_int epochs_bound then failwith "Max epochs is higher than bound";
  Optimise.Checkpoint.Custom
  (fun state ->
    if Float.(of_int state.current_batch /. of_int state.batches_per_epoch) >= epochs
    then state.stop <- true)

let oos_epoch_stopping ~patience ~tolerance ~oos_size oos_losses nn epochs =
  oos_losses := Array.make epochs Float.infinity;
  let out_of_sample = generate_data ~dims oos_size in
  let x = Algodiff.Arr out_of_sample.x
  and y = Algodiff.Arr out_of_sample.y in
  !oos_losses.(0) <- Graph.run x nn |> Model.training_objective ~dims y |> Algodiff.unpack_flt |> (fun z -> z /. Float.of_int (Arr.numel out_of_sample.y));
  let running_min = ref Float.infinity in
  let running_min_ind = ref 0 in
  Optimise.Checkpoint.Custom
  (fun (state : Optimise.Checkpoint.state) ->
    let cur_batch = state.current_batch in
    let epoch = cur_batch / state.batches_per_epoch in
    if (cur_batch mod state.batches_per_epoch = 0)
    then (
      let oos_loss =
        let ad_loss =
        Model.training_objective ~dims
        y
        (Graph.run x nn)
        in
        Algodiff.unpack_elt ad_loss
        |> fun z -> z /. Float.of_int (Arr.numel out_of_sample.y)
      in
      !oos_losses.(epoch) <- oos_loss;
      if oos_loss < !running_min
      then (running_min := oos_loss; running_min_ind := epoch);
      if (epoch >= patience && (oos_loss /. !running_min -. 1. > tolerance || !running_min_ind + 2 * patience < epoch))
      then state.stop <- true))


let train_ stopping_rule batch_size train_obj =
  let { arch=_; nn; train_data} = train_obj in
  let dims = NN.get_dims nn in
  let epochs = 500. in
  let params =
    let open Optimise in
    let batch =
      if batch_size = Arr.numel train_data.y
      then Batch.Full else Batch.(Sample batch_size)
    and loss = Loss.(Custom (Model.training_objective ~dims))
    and learning_rate = Learning_Rate.(default (Adam (0.001, 0.9, 0.999)))
    and stopping = Stopping.(default (Const 0.))
    and verbosity = true
    and checkpoint = stopping_rule nn (Float.to_int epochs)
      (* Continue while improved out of sample performance *)
      
      (* Compare loss to out of sample loss.
       * Stop if difference is some %
      (fun (state : Optimise.D.Checkpoint.state) ->
        let cur_batch = state.current_batch in
        if (cur_batch mod 100 = 0 && cur_batch >= 200)
        then (
          let oos_loss =
            let ad_loss =
            All_steps.training_objective ~dims
            y
            (Graph.run x nn)
            in
            Algodiff.unpack_elt ad_loss
            |> fun z -> z /. (float_of_int oos_size)
          in
          let cur_loss = Algodiff.unpack_elt state.loss.(cur_batch-1) in
          print_float oos_loss; print_newline ();
          print_float cur_loss; print_newline ();
          state.stop <- oos_loss < 0.95 *. cur_loss)) *)
    in
    Params.config epochs
    ~batch
    ~loss
    ~learning_rate
    ~stopping
    ~verbosity
    ~checkpoint
  in
  Gc.full_major ();
  let train_state = Graph.train
    ~params
    ~init_model:false
    nn
    train_data.x
    train_data.y
  in
  train_state


let test_performance ?sample_size ~seed {arch=_; nn; train_data} =
  let dims = Data.get_dims train_data in
  let trainperf = performance dims nn train_data in

  Seed.run seed;
  let sample_size =
    match sample_size with
    | None -> Arr.row_num train_data.y
    | Some x -> x
  in

  let test_data = generate_data ~dims sample_size in
  let testperf = performance dims nn test_data in
  let testlog = { train=trainperf; test=testperf} in
  print_testlog testlog;
  Printf.printf "#params: %i\n" (count_params nn);
  testlog
end
