open Owl
open Neural.D
module Graph = Graph
module Algodiff = Algodiff
module Optimise = Optimise

type accum =
  | Nothing
  | Arithmetic
  | Geometric
  | Lambda of int array * (Algodiff.t array -> Algodiff.t)

type layer_init = Logging.layer_init =
  | Standard
  | He
  [@@deriving yojson]

type arch =
  { dims : int
  ; widths : int array
  ; init : layer_init } [@@deriving yojson]

module type Model = sig
  val dims : int
  val noise_accum : accum
  val action_accum : accum
  val control_out_dim : dims:int -> int
  val true_optimizer : Arr.arr -> Arr.arr (* input: one sample *)
  val dynamics : Arr.arr -> Arr.arr -> Arr.arr -> Arr.arr (* maps control and x to y *)
  val obj_fun_avg : dims:int -> Arr.arr -> Arr.arr -> float
  val training_objective : dims:int -> Algodiff.t -> Algodiff.t -> Algodiff.t
  val generate_x : dims:int -> int -> Arr.arr
  val generate_y : dims:int -> Arr.arr -> Arr.arr
end

module Data = struct
  type t = { x : Arr.arr; y : Arr.arr }

  let get_dims d = (Arr.shape d.x).(1)
  let get_dims_of_entry_shape s = s.(0)

  let load_from_files xname yname =
    match Owl.Arr.(load xname, load yname)  with
    | exception Sys_error _ ->
        None
    | x, y ->
        Some { x; y }

  let save_to_files train_data xname yname =
    Owl.Arr.save ~out:xname train_data.x;
    Owl.Arr.save ~out:yname train_data.y
end

module Make (Model : Model) = struct
module Data_gen = struct
  type t = Data.t = { x : Arr.arr; y : Arr.arr }
  let generate_data ~dims n =
    let x = Model.generate_x ~dims n in
    let y = Model.generate_y ~dims x in
    { x; y }
end

let get_dims nn = Data.get_dims_of_entry_shape (Graph.input_shape nn)

let slice_node ?name ?act_typ ~out_shape sl inp =
  let open Graph in
  let lifted_sl = []::sl in
  lambda_array ?name ?act_typ out_shape 
    (fun x -> Algodiff.Maths.(get_slice lifted_sl (Array.get x 0))) [|inp|]

let make ~dims ~init:layer_init widths =
  let open Graph in
  let open Neuron in
  (*let open Algodiff in*)
  let inp = input [| dims; 2 |] in
  let d1 = slice_node ~name:"d_1" ~out_shape:[| dims; 1 |] [[]; [0]] inp
  and d2 = slice_node ~name:"d_2" ~out_shape:[| dims; 1 |] [[]; [1]] inp in
  let act_typ = Activation.Relu
  and init_typ =
    match layer_init with 
    | Standard -> Init.Standard
    | He -> Init.(Custom
      (fun s ->
        let fan_in, _ = calc_fans s in
        let r = sqrt (6. /. fan_in) in
        let open Algodiff in
        Arr.uniform ~a:(A.float_to_elt (-.r)) ~b:(A.float_to_elt r) s))
  in
  let depth = Array.length widths in
  let a1 = Array.fold_left
      (fun n width ->
        n |> fully_connected ~act_typ ~init_typ width)
      d1 widths
      |> fully_connected
        ~name:"a_1"
        ~act_typ:Activation.None
        ~init_typ:Init.(Uniform (0., 2./.(Int.to_float widths.(depth-1))))
        (Model.control_out_dim ~dims)
      |> reshape [| Model.control_out_dim ~dims; 1 |] (* slice_node workaround *)
  in
  let action_res = match Model.noise_accum with
  | Nothing -> a1
  | Arithmetic -> add [| a1; d2 |]
  | Geometric -> mul [| a1; d2 |]
  | Lambda (shape, f) -> lambda_array shape f [| a1; d2 |]
  in
  let accum = match Model.action_accum with
  | Nothing -> action_res
  | Arithmetic -> add [| action_res; d1 |]
  | Geometric -> mul [| action_res; d1 |]
  | Lambda (shape, f) -> lambda_array shape f [| action_res; d1 |]
  in
  get_network accum

type load_error = [ `File_not_found of string | `Wrong_arch | `Parse_error of string ]
let load weights_file arch_file
  : (arch * Graph.network, load_error) result
  =
  let (let*) = Result.bind in
  let* { dims; widths; init } as arch =
    match arch_of_yojson @@ Yojson.Safe.from_file arch_file with
    | Ok _ as x -> x
    | Error s -> Error (`Parse_error s)
    | exception Sys_error _ -> Error (`File_not_found arch_file)
  in
  let nn = make ~dims ~init widths in
  match Graph.load_weights nn weights_file with
  | exception Sys_error _ ->
      Result.Error (`File_not_found weights_file)
  | exception Not_found ->
      Result.Error `Wrong_arch
  | _ -> Result.Ok (arch, nn)
end

let count_params nn =
  let open Graph in
  Owl_utils.Array.filter (fun n -> match n.neuron with Neuron.FullyConnected _ -> true | _ -> false) nn.topo
  |> Array.map (fun n ->
      let p_cnt l =
        match l with
        | Neuron.FullyConnected l ->
          let wm = Array.fold_left (fun a b -> a * b) 1 l.in_shape in
          let wn = l.out_shape.(0) in
          let bn = l.out_shape.(0) in
          (wm * wn) + bn
        | _ -> 0
      in
      p_cnt n.neuron)
  |> Array.fold_left (fun acc x -> acc + x) 0
