docker_compile:
	sudo docker build --target owl_compile -t owl_compile -f Dockerfile.owl .
	echo "Run with"
	echo 'docker run --rm -it -v "$$PWD":/home/max/workdir owl_run bash -c "dune build ..."'

docker_run:
	sudo docker build --target owl_run -t owl_run -f Dockerfile.owl .
	echo "Run with"
	echo 'docker run --rm -it -v "$$PWD":/home/max/workdir owl_run _build/default/...'

